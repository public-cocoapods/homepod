# Homepod

[![CI Status](https://img.shields.io/travis/windtalkerz/Homepod.svg?style=flat)](https://travis-ci.org/windtalkerz/Homepod)
[![Version](https://img.shields.io/cocoapods/v/Homepod.svg?style=flat)](https://cocoapods.org/pods/Homepod)
[![License](https://img.shields.io/cocoapods/l/Homepod.svg?style=flat)](https://cocoapods.org/pods/Homepod)
[![Platform](https://img.shields.io/cocoapods/p/Homepod.svg?style=flat)](https://cocoapods.org/pods/Homepod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Homepod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Homepod'
```

## Author

windtalkerz, windtalkerz@me.com

## License

Homepod is available under the MIT license. See the LICENSE file for more info.
