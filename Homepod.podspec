#
# Be sure to run `pod lib lint Homepod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Homepod'
  s.version          = '0.0.1'
  s.summary          = 'homemade cocoapod'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
the start of a new day dawn
                       DESC

  s.homepage         = 'https://gitlab.com/public-cocoapods/homepod/blob/master/README.md'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'windtalkerz' => 'windtalkerz@me.com' }
  s.source           = { :git => 'https://gitlab.com/public-cocoapods/homepod.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.1'
  s.swift_version = '5.0'

  s.source_files = 'Homepod/Classes/**/*'
  
  # s.resource_bundles = {
  #   'Homepod' => ['Homepod/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
